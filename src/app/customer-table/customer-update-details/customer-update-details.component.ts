import { Component, OnInit, Inject } from '@angular/core';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { FormControl, FormGroup, FormBuilder } from '@angular/forms';

@Component({

  selector: 'app-customer-update-details',

  templateUrl: './customer-update-details.component.html',

  styleUrls: ['./customer-update-details.component.css']

})

export class CustomerUpdateDetailsComponent implements OnInit {

 

  constructor(public dialogRef: MatDialogRef<CustomerUpdateDetailsComponent>, private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any) { }

 

  onNoClick(): void {

    console.log(this.data);

   

    this.dialogRef.close();

  }

 

 

  userRegForm = this.fb.group({

   name: [this.data.name],

   email: [this.data.email],

   phone: [this.data.phone],

   age: [this.data.age],

   address: this.fb.group({

     city: [this.data.city],

     pincode: [this.data.pincode]

   })

})

 ngOnInit() {

}

 

}