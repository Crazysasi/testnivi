
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerTableComponent } from './customer-table/customer-table.component';
import { AgGridModule } from 'ag-grid-angular';
import { CustomisedEditButtonComponent } from './customised-edit-button/customised-edit-button.component';
import { MatModule } from '../mat/mat.module';
import { ReactiveFormsModule } from '@angular/forms';
import { GridDataService } from '../shared/grid-data.service';
import { CustomerUpdateDetailsComponent } from './customer-update-details/customer-update-details.component';

@NgModule({
  declarations: [CustomerTableComponent, CustomisedEditButtonComponent, CustomerUpdateDetailsComponent],
  imports: [
    CommonModule,
    MatModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([CustomisedEditButtonComponent]
    )
  ],
  exports: [
    CustomerTableComponent,
  ],
  providers: [
  ],
  entryComponents: [CustomerUpdateDetailsComponent]
})

export class CustomerTableModule { }


