import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductTableComponent } from './product-table/product-table.component';
import { ProductEditButtonComponent } from './product-edit-button/product-edit-button.component';
import { ProductUpdateDetailsComponent } from './product-update-details/product-update-details.component';
import { MatModule } from '../mat/mat.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { ProductUserDetailsComponent } from './product-user-details/product-user-details.component';

@NgModule({
  declarations: [ProductTableComponent, ProductEditButtonComponent, ProductUpdateDetailsComponent, ProductUserDetailsComponent],
  imports: [
    CommonModule,
    MatModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([ProductEditButtonComponent]
      )
  ],exports :[ProductTableComponent,],
  entryComponents: [ProductUpdateDetailsComponent, ProductUserDetailsComponent]
})

export class ProductTableModule { }